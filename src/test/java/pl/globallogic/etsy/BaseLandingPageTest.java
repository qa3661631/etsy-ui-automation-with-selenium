package pl.globallogic.etsy;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import pl.globallogic.configuration.OurWebDriverManager;
import pl.globallogic.etsy.features.pageobjects.InvalidSearchResultPage;
import pl.globallogic.etsy.features.pageobjects.LandingPage;

public class BaseLandingPageTest {
    static Logger logger = LoggerFactory.getLogger(BaseLandingPageTest.class);
    protected WebDriver driver;
    protected LandingPage landingPage;
    protected InvalidSearchResultPage invalidSearchResultPage;

    @BeforeClass
    public void globalSetUp() {
        // System.setProperty("webdriver.chrome.driver", "D:/");
        // WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void testSetUp() {
        System.setProperty("browser", "chrome");
        driver = OurWebDriverManager.getConfiguredWebDriver();
        logger.info("Selenium web driver configured");
        landingPage = new LandingPage(driver);
        landingPage.goTo();
        landingPage.acceptDefaultPrivacyPolicy();
    }

    @AfterClass
    public void globalCleanUp(){
        logger.info("******* Test suit execution completed *******");
    }


    @AfterMethod
    public void testCleanUp() {
        logger.info("Selenium webdriver quited");
        driver.quit();
    }
}
