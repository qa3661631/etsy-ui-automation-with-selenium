package pl.globallogic.etsy.features;

import org.testng.Assert;
import org.testng.annotations.*;
import pl.globallogic.etsy.BaseLandingPageTest;
import pl.globallogic.etsy.features.pageobjects.InvalidSearchResultPage;


public class SearchFeatureVerificationTest extends BaseLandingPageTest {

    @Test
    public void shouldDisplaySearchResultsForValidQuery() {
        String validQuery = "leather bag";
        landingPage.searchFor(validQuery);
        Assert.assertTrue(landingPage.isSearchResultValidFor(validQuery));
    }

    @Test
    public void shouldDisplayNotFoundPageForInvalidQuery() {
        String queryForInvalidSearchResultPage =
                "naosnfndsnoSNDUOSNDO,2810m8asd2121n0oahsd098.ass;d[]amfdi21asdadsa\"\"\"EQF\"23212121221212121212121211122112122112212121121221";
        landingPage.searchFor(queryForInvalidSearchResultPage);
        invalidSearchResultPage = new InvalidSearchResultPage(driver);
        Assert.assertTrue(invalidSearchResultPage.isVisible());
    }

}