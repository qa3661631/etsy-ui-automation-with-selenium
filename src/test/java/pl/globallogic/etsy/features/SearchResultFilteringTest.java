package pl.globallogic.etsy.features;

import org.testng.annotations.Test;
import pl.globallogic.etsy.BaseLandingPageTest;

public class SearchResultFilteringTest extends BaseLandingPageTest {

    //landing page
    //search result page


    //Filter search result by price -> price didn't is lower than price value for filtering
    @Test
    public void searchResultPriceFitPriceRangeAfterFiltering() {
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //expand filtering panel
        //select required filters (by price)
        //apply selected filters
        //wait for search result to be filtered
        //verify item price is in range (one or more items have a price from specified range)
    }

    //Filter search result by free shipping -> free shipping tag need to be present on all items
    @Test
    public void freeShippingTagNeedToBePresentAfterFiltering() {
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //expand filtering panel
        //select required filters (by free shipping)
        //apply selected filters
        //wait for search result to be filtered

        //verify free shipping tag/label is present on search result page

    }

    //Already filtered search results not changed when canceling filtering
    @Test
    public void searchResultPageContentNotChangedAfterCanceling() {
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //get 1st result title to be used in verification (after canceling shoud be the same)
        //expand filtering panel
        //wait for search result to be filtered
        //cancel filter application
        //get 1st result and compare
    }

    //Search results filtered by color -> check if color from filter can be selected for purchase
    @Test
    public void filteredOptionShouldBeAvailableInItemDetailsViewAfterFilteringByColor() {
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //expand filtering panel
        //select required filters (by color)
        //apply selected filters
        //wait for search result to be filtered
        //go to 1st result item details view
        //verify color selection element contains required color
    }

    //Ship to country filtering -> check if item details contains expected country for shipping
    @Test
    public void filteredShippingCountryShouldBePresentInShippingDestinationOptions() {
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //expand filtering panel
        //select required filters (by shipping country)
        //apply selected filters
        //wait for search result to be filtered
        //go to 1st result item details view
        //verify shipping destination selection element contains required country
    }
}
