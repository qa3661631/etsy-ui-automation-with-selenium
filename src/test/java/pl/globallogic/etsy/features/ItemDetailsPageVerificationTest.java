package pl.globallogic.etsy.features;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ItemDetailsPageVerificationTest {

    //landing page
    //item details page

    @BeforeMethod
    public void testSetUp(){
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //go to 1st result item details view
    }

    @Test
    public void containsItemImage(){
        //verify item image element visibility
    }

    @Test
    public void containsItemDescription(){
        //verify item description text is not empty
    }

    @Test
    public void shouldHaveAddToCardButton(){
        //verify add to cart button is visible
    }
}
