package pl.globallogic.etsy.features;

import org.testng.annotations.Test;
import pl.globallogic.etsy.BaseLandingPageTest;

public class SearchResultsSortingTest extends BaseLandingPageTest {
    //Sort by lowest price -> verify that number of items (one of the result rows) sorted in ascending order
    @Test
    public void pricesShouldBeOrderedAccordingToSortingCriteria(){
        //go to landing page
        //search for item
        //wait for search result to be loaded
        //select required sorting order (lowest price)
        //apply sorting
        //wait for search result to be sorted
        //verify item prices sorting order (take 4 consecutive items and check if price[i] < price[i+1])
    }
}
