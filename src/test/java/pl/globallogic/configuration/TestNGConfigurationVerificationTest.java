package pl.globallogic.configuration;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNGConfigurationVerificationTest {
    @BeforeClass
    public void globalSetUp(){
        // System.setProperty("webdriver.chrome.driver", "D:/");
    }
    @AfterClass
    public void globalCleanUp(){
        System.out.println("Test suite execution completed");
    }
    @Test
    public void shouldAlwaysBeTrue() {
        //arrange
        int n1 = 4;
        int n2 = 4;
        //act
        n1 = n1 + 10;
        //assert
        Assert.assertNotEquals(n1, n2);
    }
}
