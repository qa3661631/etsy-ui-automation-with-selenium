package pl.globallogic.configuration;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SeleniumConfigurationVerificationTest {
    WebDriver driver;
    @BeforeClass
    public void globalSetUp(){
        // System.setProperty("webdriver.chrome.driver", "D:/");
        WebDriverManager.chromedriver().setup();
    }
    @AfterClass
    public void globalCleanUp(){
        System.out.println("Test suite execution completed");
    }
    @Test
    public void shouldNavigateToGoogleComAndVerifyPageTitle(){
        //arrange
        driver = new ChromeDriver();
        String expectedPageTitle = "Google";
        //act
        driver.get("https://www.google.com");
        String actualPageTitle = driver.getTitle();
        driver.quit();
        //assert
        Assert.assertEquals(expectedPageTitle, actualPageTitle);
    }
}
