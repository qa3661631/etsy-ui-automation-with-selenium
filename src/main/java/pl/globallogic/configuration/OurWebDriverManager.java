package pl.globallogic.configuration;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class OurWebDriverManager {
    public static WebDriver getConfiguredWebDriver(){
        String browserType = System.getProperty("browser", "chrome").toLowerCase();
        return switch(browserType){
            case "chrome":
                WebDriverManager.chromedriver().setup();
                //ChromeOptions opt = new ChromeOptions();
                //opt.addArguments("--headless=new");
                yield new ChromeDriver();
                //yield new ChromeDriver(opt);
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                yield new FirefoxDriver();
            default:
                yield new ChromeDriver();
        };
    }
}
